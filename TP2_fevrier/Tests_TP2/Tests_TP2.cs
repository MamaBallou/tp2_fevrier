﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;

using TP2_fevrier;

namespace Tests_TP2
{
    [TestClass]
    public class Tests_TP2
    {
        [TestMethod]
        public void TestMajuscule()
        {
            Assert.IsTrue(Program.CommenceParMajuscule("Au revoir"));
            Assert.IsFalse(Program.CommenceParMajuscule("bonjour."));
            Assert.IsFalse(Program.CommenceParMajuscule(" rebonjour."));
        }

        [TestMethod]
        public void TestPoint()
        {
            Assert.IsTrue(Program.FiniParPoint("azerty."));
            Assert.IsFalse(Program.FiniParPoint("Qwerty"));
            Assert.IsFalse(Program.FiniParPoint("qwerty. "));
        }

        [TestMethod]
        public void Test_ListerValeursIdentiques()
        {
            int[] tableau = new int[10];
            for (int compteur = 0; compteur < 10; compteur++) tableau[compteur] = 3;
            Assert.AreEqual("", Program.VerifTableau(tableau, 2));
            Assert.AreNotEqual(",", Program.VerifTableau(tableau, 2));
            tableau[5] = 5;
            Assert.AreEqual("6", Program.VerifTableau(tableau, 5));
            Assert.AreNotEqual("6,", Program.VerifTableau(tableau, 5));
            tableau[6] = 5;
            Assert.AreEqual("6, 7", Program.VerifTableau(tableau, 5));
            Assert.AreNotEqual("6, 7,", Program.VerifTableau(tableau, 5));
        }
    }
}
