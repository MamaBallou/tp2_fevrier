﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TP2_fevrier
{
    public class Program
    {
        public static bool CommenceParMajuscule(string phrase)
        {
            if ((phrase[0] >= 'A') && (phrase[0] <= 'Z')) return true;
            return false;
        }

        public static bool FiniParPoint(string phrase)
        {
            int longueur_phrase;
            longueur_phrase = phrase.Length;
            if (phrase[longueur_phrase - 1] == '.') return true;
            return false;
        }

        public static void Saisie_tableau(int[] tableau_valeurs)
        {
            string[] string_tableau_valeurs = new string[10];
            Console.WriteLine("Entrer une liste de 10 nombre entiers :");
            for (int compteur = 0; compteur < 10; compteur++)
            {
                do
                {
                    Console.Write("Nombre {0} : ", compteur + 1);
                    string_tableau_valeurs[compteur] = Console.ReadLine();
                } while (!int.TryParse(string_tableau_valeurs[compteur], out tableau_valeurs[compteur]));
            }
        }

        public static string VerifTableau(int[] tableauValeurs, int valeurCherchee)
        {
            string listeEmplacements = "";
            for (int compteur = 0; compteur < 10; compteur ++)
            {
                if (tableauValeurs[compteur] == valeurCherchee)
                {
                    if (listeEmplacements == "") listeEmplacements += Convert.ToString(compteur + 1);
                    else listeEmplacements = listeEmplacements + ", " + Convert.ToString(compteur + 1);
                }
            }
            return listeEmplacements;
        }

        public static void Affichage(string liste_rangs, int valeur_cherchee)
        {
            if (liste_rangs == "") Console.WriteLine("Aucune valeur de correspond à la valeur recherchée");
            else Console.WriteLine("{0} est la {1} -eme valeur saisie.", valeur_cherchee, liste_rangs);
        }

        static void Main(string[] args)
        {
            int num_exercice;
            string string_num_exercice;
            do
            {
                do
                {
                    Console.WriteLine("Bonjour, souhaitez vous écrire une phrase (1) ou bien un liste de nombre entiers (2) ?");
                    Console.WriteLine("En revanche, si vous souhaitez quitter tapez '-1'");
                    string_num_exercice = Console.ReadLine();
                } while (!int.TryParse(string_num_exercice, out num_exercice));
                switch (num_exercice)
                {
                    case 1:
                        string phrase;
                        Console.WriteLine("Veuillez saisir une phrase : ");
                        phrase = Console.ReadLine();
                        if (CommenceParMajuscule(phrase)) Console.WriteLine("Cette phrase commence par une majuscule.");
                        else Console.WriteLine("Cette phrase ne commence pas par une majuscule.");
                        if (FiniParPoint(phrase)) Console.WriteLine("Cette phrase se termine par un point.");
                        else Console.WriteLine("Cette phrase ne se termine pas par un point.");
                        break;

                    case 2:
                        string string_valeur_cherchee, listeRangs = "";
                        int[] tableau_valeurs = new int[10];
                        int valeur_cherchee;

                        Saisie_tableau(tableau_valeurs);
                        do
                        {
                            Console.Write("Saisir la valeur à rechercher dans la liste précédement saisie : ");
                            string_valeur_cherchee = Console.ReadLine();
                        } while (!int.TryParse(string_valeur_cherchee, out valeur_cherchee));
                        listeRangs = VerifTableau(tableau_valeurs, valeur_cherchee);
                        Affichage(listeRangs, valeur_cherchee);
                        break;
                    case -1:
                        Console.WriteLine("Au revoir !");
                        break;

                    default:
                        Console.WriteLine("Cet exercice n'existe pas !");
                        break;
                }
            } while (num_exercice != -1);
            Console.ReadKey();

        }
    }
}
